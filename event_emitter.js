class EventEmitter {
  constructor() {
    this.listenerMap = {};
  }

  on(eventName, observer) {
    if (this.listenerMap[eventName]) {
      this.listenerMap[eventName].push(observer);
    } else {
      this.listenerMap[eventName] = [observer];
    }
  }
  emit(eventName, context) {
    this.listenerMap[eventName].forEach(callback => {
      callback(context);
    });
  }
}
