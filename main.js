let eventEmitter = new EventEmitter();
let countrySelect = $('.country');
let citySelect = $('.city');
let weatherSpan = $('.weather');
let flagImage = $('.flagImage');
let countryNames = countries.map(country => country.name);
let countryCityMap = {};
let countryFlagImageUrlMap = {};
let cityWeatherMap = {};

countries.forEach(country => {
  let cities = country.cities;
  countryCityMap[country.name] = cities.map(city => city.name);
  countryFlagImageUrlMap[country.name] = country.flagImage;
  cities.forEach(city => {
    cityWeatherMap[city.name] = city.weather;
  });
});

populateCountryOptions(countryNames);
populateCityOptions([]);

countrySelect.change(onCountrySelect);
citySelect.change(onCitySelect);

function onCountrySelect(e) {
  let selectedCountry = countrySelect.val();
  eventEmitter.emit('country_changed', selectedCountry);
}

function onCitySelect() {
  let selectedCity = citySelect.val();
  eventEmitter.emit('city_changed', selectedCity);
}

eventEmitter.on('country_changed', function(country) {
  let correspondingCity = countryCityMap[country];
  populateCityOptions(correspondingCity);
});

eventEmitter.on('country_changed', function(country) {
  renderWeather('');
});

eventEmitter.on('country_changed', function(country) {
  renderFlagImage(country);
});

eventEmitter.on('city_changed', function(city) {
  renderWeather(cityWeatherMap[city]);
});

function populateCountryOptions(countries) {
  countrySelect.empty();
  countrySelect.append($(`<option value="">select country</option>`));
  countries.forEach(country => {
    let option = $(`<option value=${country}>${country}</option>`);
    countrySelect.append(option);
  });
}

function populateCityOptions(cities) {
  citySelect.empty();
  citySelect.append($(`<option value="">select city</option>`));
  cities.forEach(city => {
    let option = $(`<option value=${city}>${city}</option>`);
    citySelect.append(option);
  });
}

function renderWeather(weather) {
  weatherSpan.text(weather);
}

function renderFlagImage(country) {
  flagImage.attr('src', countryFlagImageUrlMap[country]);
}

